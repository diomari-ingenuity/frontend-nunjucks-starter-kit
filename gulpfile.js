'use strict';
var gulp = require('gulp'),
    prefix = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    nunjucksRender = require('gulp-nunjucks-render'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    size = require('gulp-size'),
    sourcemaps = require('gulp-sourcemaps'),
    util = require('gulp-util'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    wiredep = require('wiredep').stream;

// Task for preparing sass files
gulp.task('sass', function() {
  return gulp.src('sass/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(size({
      gzip: true,
      showFiles: true
    }))
    .pipe(prefix({
      browsers: ['last 2 versions'],
      cascase: false
    }))
    .pipe(rename('main.css'))
    .pipe(gulp.dest('./app/static/css'))
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./app/static/css'))
    .pipe(browserSync.stream());
});

// Task for preparing templates
gulp.task('nunjucks', ['bower'], function() {
    return gulp.src('app/pages/**/*.+(html)')
    // Renders template with nunjucks
    .pipe(nunjucksRender({
        path: ['app/templates']
      }))
    // output files in app folder
    .pipe(gulp.dest('app'))
});

gulp.task('bower', function() {
    return gulp.src('app/templates/*.html')
        .pipe(wiredep({
            optional: 'configuration',
            goes: 'here'
        }))
        .pipe(gulp.dest('app/templates'))
})

// Task for hosting the local server
gulp.task('serve', ['sass', 'nunjucks'],function() {
    browserSync.init({
        server: {
          baseDir: "./app/"
        }
    });
    
    gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch('./app/templates/**/*.html', ['nunjucks']);
    gulp.watch('./app/pages/**.html', ['nunjucks']);
    gulp.watch("./app/**.html").on("change", browserSync.reload);
});

// Prepare files for production
// gulp.task('build', function() {
    
// });

# README #


### Frontend Starter Kit Repository ###

This starter kit will serve as a base repository for front-end projects in Ingenuity.

Advantages of using this base kit:
* It has template inheritance, you don't have to copy paste all html code from one page to another. It will also improve code maintainability since you don't need to modify each template if there's a change on header, footer or any generic changes on the template(DRY)
* Everytime you make changes on the source code the browser will automatically reload
* Built-in SASS compilation

This starter kit heavily relies on NodeJS. It uses Nunjucks as its templating engine. Gulp as its task runner. Sass as its CSS preprocessor. To learn more about these tools kindly check the links below.

* [Learn Nunjucks](https://mozilla.github.io/nunjucks/)
* [Learn Gulp](http://gulpjs.com/)
* [Learn Sass](http://sass-lang.com/)

### How do I get set up? ###

Make sure you have nodejs on your system. To check if you have nodejs installed go to terminal and type this command `node -v` if a version number does not come out install nodejs first.

Kindly check [Nodejs](https://mozilla.github.io/nunjucks/) official site for installation details.

After cloning the repo run these commands:

Install using NPM
```
npm intall && bower install
```

UPDATE: For faster installation of dependencies you can use yarn instead of npm(https://yarnpkg.com/).

Install using yarn
```
yarn install && bower install
```


This would install all needed dependencies and plugins for the project to run. After running the mentioned command succesfully you can now use Gulp to host the templates.

### Gulp Commands ###

`gulp serve` - This will create a local server instance and prepares the styles and templates along the way.


`gulp build` - (Not yet implemented) This will generate the compiled and minified version of the templates in the `dir` folder.

### Who do I talk to? ###

If you have questions kindly contact these folks :)

* diomari@ingenuity.ph
